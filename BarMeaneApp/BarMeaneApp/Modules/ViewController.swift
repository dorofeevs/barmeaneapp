// ViewController.swift
// Copyright © RoadMap. All rights reserved.

import UIKit

/// Root view controller.
final class ViewController: UIViewController {
    // MARK: - Life cycle.

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
